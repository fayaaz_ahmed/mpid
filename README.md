# README #

### What is this repository for? ###

* MPiD

### How do I get set up? ###

For the raspberry pi, the shell script below should install it from  a new raspbian installation. It should work on most debian linuxes, not tested though!

https://gist.github.com/fayaaz/6bbc569a2f17c821f511

Running the commands below should do the bulk of the install for you.

(Currently does not include upmpdcli)

```
#!bash

wget https://gist.githubusercontent.com/fayaaz/6bbc569a2f17c821f511/raw/mpidinstall.sh
chmod 775 mpidinstall.sh
sudo ./mpidinstall.sh

```

**The script installs:**

* mpd
* nginx
* network/usb file utilities (cifs-utils, usbmount)
* audio utilities (alsa-utils)
* required python libraries (Flask, python-mpd2, wpa-config)
* supervisor+gunicorn
* MPiD by cloning into /var/www/mpid/

This should give you a working environment.

If you need to do python debugging:

```
supervisorctl stop gunicorn
python /var/www/mpid/mpid.py
```

(These may need to run as root depending on what you're doing)

### Contribution guidelines ###

* Someone help me!