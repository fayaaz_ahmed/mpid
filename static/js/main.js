;(function(){

			// Menu settings
			$('#menuToggle, .menu-close').on('click', function(){
				
				$('body').toggleClass('body-push-toleft');
					
				if ($('body').hasClass('body-push-toleft')){
					$('#theMenu').animate({'right':'0'}, 200);
					$('#menuToggle').animate({'right':'120px'}, 200);
				}else{
					$('#theMenu').animate({'right':'-160px'}, 200);
					$('#menuToggle').animate({'right':'20px'}, 200);
				}
			});
			

})(jQuery);