//on document ready, run updateStatus
$(document).ready (function(){
   updateStatus();
   getPlaylist();
   toggleSidebarLinks();
    $(".dragger").attr('data-toggle','tooltip').attr('data-placement', 'right');
    $("div.progress").attr('data-toggle','tooltip').attr('data-placement', 'right');
});

$(window).resize(function() {
  toggleSidebarLinks();
});


// Submit LastFM details
$('#last-fm-form').on('submit', function(e){
	e.preventDefault();
	var username = $("#usernameInput").val();
	var password = $("#passwordInput").val();
	$.ajax({
		url: 'settings/lastfm',
		type: 'POST',
		dataType: 'json',
		data: {'username' : username,
				'password': password
			},
		success: function(data){
			// To be filled in by chosen notification system
		}
	});
});

// Submit WiFi details
$('#wifi-form').on('submit', function(e){
	e.preventDefault();
	var ssid = $("#ssidInput").val();
	var passphrase = $("#passphraseInput").val();
	$.ajax({
		url: 'settings/wifi',
		type: 'POST',
		dataType: 'json',
		data: {
			'ssid' : ssid,
			'passphrase': passphrase
			},
		success: function(data){
			// To be filled in by chosen notification system
		}
	});
});

$("body").delegate( '[data-toggle="tooltip"]', 'hover', function(){
	$(this).tooltip();
});

function toggleSidebarLinks(){
	if ($("#playlist").offset().top !== $("#library").offset().top){
		$("#playlistlink").show();
	} else{
		$("#playlistlink").hide();
	}
}
//


//
//==================CONTROLS==============================//
//

/////// Keyboard controls /////////////

$(document).keypress(function(event){
 	
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '32'){
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: '/control/',
			dataType: 'json',
			data: {'method':'playpause'},
			success: function(data){
				updateStatusDOM(data);
				}
				
		});

	}
 
});
 

//Control for PROGRESS
$("body").delegate( "div.progress", 'click', function(event){
	var offset = $(this).offset();
	var clickedPosition = event.clientX - offset.left;
	var fullBarWidth = $("div.progress").width();
	var songLength = $("div.progress").attr("song-length");
	var songPos = $("div.progress").attr("song-pos");
	var time = Math.round(clickedPosition*songLength/fullBarWidth);
  	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'seek',
				'data-time': time,
				'data-song' : songPos,
				},
		success: function(data){
			updateStatusDOM(data);
			}

	});
});

//Tooltip for PROGRESS
$("body").delegate( "div.progress", 'mousemove', function(event){
	var offset = $(this).offset();
	var clickedPosition = event.clientX - offset.left;
	var fullBarWidth = $("div.progress").width();
	var songLength = $("div.progress").attr("song-length");
	var time = Math.round(clickedPosition*songLength/fullBarWidth);
	var timeString = Math.round(time/60) + ":" + ("0" + time%60).slice(-2)
	$(this).tooltip('destroy')
    $(this).tooltip({title: timeString});
	$(this).tooltip('show')
	})

//Control for STOP
$("body").delegate( "i.control-stop", 'click', function(){
	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'stop'},
		success: function(data){
			updateStatusDOM(data);
			}
			
	});
		
});

$("body").delegate("a.clip", 'click', function(event){
	event.preventDefault();
	var ytstring = $(this).attr('href').replace("http://www.youtube.com/watch?v=", "").replace("&feature=youtube_gdata_player","");
	playlistMethodSend('youtube', {'ytstring':ytstring});
});

//Control for PLAY/PAUSE
$("body").delegate( "i.control-status", 'click', function(){
	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'playpause'},
		success: function(data){
			updateStatusDOM(data);
			}
			
	});
		
});


//Control for next song
$("body").delegate( "i.control-next", 'click', function(){
	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'next'},
		success: function(data){
			updateStatusDOM(data);
			}
	});
});

//Control for previous song
$("body").delegate( "i.control-previous", 'click', function(){
	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'previous'},
		success: function(data){
			updateStatusDOM(data);
			}
			
	});
		
});

//Control for update library
$("body").delegate('i.update-mpd-library', 'click', function(){
	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'update'},
		success: function(data){
			updateStatusDOM(data);
		}
	});
});

//Control for SHUFFLE
$("body").delegate( "i.shuffle-toggle", 'click', function(){
	if($(".shuffle-toggle").hasClass("shuffle-on")){
		var shuffleStatus = 0;
		}
	else{
		var shuffleStatus = 1;
	}
	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'shuffle','data':shuffleStatus},
		success: function(data){
			updateStatusDOM(data);
			}

	});

});

//show/hide volume slider
$("body").delegate(".fa-volume-down.icon-play-options" , 'click', function(){
	$(".volume-hide-show").toggle();
});

//volume change
$("#ui-volume-slider").bind("slider:changed", function (event, data) {
  // The currently selected value of the slider
   $(".dragger").tooltip('destroy');
   $(".dragger").attr('title', data.value);
   $(".dragger").tooltip();
  if (data.trigger === "domDrag"){
  	$.ajax({
		type: "POST",
		url: '/control/',
		dataType: 'json',
		data: {'method':'volume','data':data.value},
		success: function(data){
			updateStatusDOM(data);
			}
			
	});
		
  }

});


//Add song to playlist
$("body").delegate("i.library-control-add", 'click', function(){
	var uri = $(this).parent().parent().attr('data-uri');
	playlistMethodSend('add', {'uri':uri});
});

//Replace and play songs to playlist
$("body").delegate("i.library-control-play", 'click', function(){
	var uri = $(this).parent().parent().attr('data-uri');
	playlistMethodSend('replace', {'uri':uri});
});


$("body").delegate(".playlist-control-remove", 'click', function(){
	var songid = $(this).parent().attr('data-id');
	playlistMethodSend('remove', {'songid':songid});
});



//Library page navigation
$("body").delegate( "div.music-library-2", 'click', function(){
	top_level = $(this).text();
 	getLibraryDIR(top_level);
});


//Go up a directory in the library
$("body").delegate("div.library-dir-up", 'click', function(){
	dir = $(this).attr("data-uri").substr(0, $(this).attr("data-uri").lastIndexOf("/"));
	getLibraryDIR(dir);
});


//Search from pressing the button
$("body").delegate("button.library-search-button", 'click', function(){
	searchLibrary($(".library-search-field").val());
});
//Search from hitting enter
$('.library-search-field').keypress(function(event){
	event.stopPropagation();
	var search_val = $(this).val();
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		searchLibrary(search_val);
	}
});


//Playlist clear
$("body").delegate(".playlist-delete", 'click', function(){
	playlistMethodSend('clear', {});
});

//Get Audio Devices
$("body").delegate(".update-audio-devices", 'click', function(){
	$.ajax({
		type: "GET",
		url: "settings/audio_devices",
		dataType: 'json',
		success: function(data){
			$(".audio-devices-select").empty();
			$.each(data, function(i, item){
				var hw = item.split(' ')[1];
				var index = item.split(' ')[3];
				$(".audio-devices-select").append("<option data-hw="+hw+" data-index="+index+">"+item+"</option>");
				$(".audio-devices-select").removeAttr("disabled");
			});

		}
	});
});

//Set Audio Devices
$("body").delegate(".set-audio-devices", 'click', function(){
	var hw = $(".audio-devices-select :selected").attr('data-hw');
	var index = $(".audio-devices-select :selected").attr('data-index');
	$.ajax({
		type: "POST",
		url: "settings/audio_devices",
		dataType: "json",
		data: {'hw':hw,'index':index},
		success: function(response){
		}
	});
	console.log();
});

$("body").delegate(".library-search-hide", 'click', function(){
	$(".library-search-container").toggle();
});

function playlistMethodSend(method, postData){
	$("div.playlist-info").empty();
	$.ajax({
		type: "POST",
		url: '/playlist/'+method,
		dataType: 'json',
		data: postData,
		success: function(data){
			updatePlaylistDOM(data);
			if(method=='replace'){
				updateStatus();
			}
				
		}
	});
};

var myInterval;
var statusUpdateInterval;


//update the status on the page from the '/status.json' ajax call
function updateStatus() {
	$.ajax({
		type: "GET",
		url: '/status.json',
		dataType: 'json',
		success: function(data){
			updateStatusDOM(data);
			setTimeout(updateStatus, 2500);
		},
		error: function(){
		}
	});
}
var songtitle;
function updateStatusDOM(mpd){
	clearInterval(myInterval);
	if(mpd.state == 'stop'){
		$("i.control-status").removeClass('control-pause').removeClass('fa-pause').addClass('control-play fa-play');
		$("div.mpd-bar").attr("aria-valuenow", "0").attr("style", "width: 0%;");
		clearInterval(myInterval);

	}
	else if(mpd.state == 'play' || 'pause'){
		var time = mpd.time.split(":");
		var played = parseInt(time[0], '10')/parseInt(time[1],'10');
		var playedTimeString = Math.floor(time[1]*played/60)+":"+("0" + Math.floor((played*time[1])%60)).slice(-2)
			+ "/" + Math.floor(time[1]/60)+":"+("0" + Math.floor((time[1])%60)).slice(-2);
		$("div.mpd-bar").attr("aria-valuenow", played).attr("style", "width: "+played*100+"%;");
		$("i.control-status").removeClass('control-play').removeClass('fa-play').addClass('control-pause fa-pause');
		$("div.progress").attr("song-length",time[1]);
		$("div.progress").attr("song-pos",mpd.song);
		if(mpd.state == 'play'){
			myInterval = setInterval(function () {
				played = played+(0.1/parseInt(time[1],'10'));
				playedTimeString = Math.floor(time[1]*played/60)+":"+("0" + Math.floor((played*time[1])%60)).slice(-2)
				+ "/" + Math.floor(time[1]/60)+":"+("0" + Math.floor((time[1])%60)).slice(-2);
				$("div.mpd-bar").attr("aria-valuenow", played).attr("style", "width: "+played*100+"%;");
				$("div.now-playing-time").text(playedTimeString);
				if(played>1){
					clearInterval(myInterval);
					updateStatus();
				}
			}, 100);
		}
		else if(mpd.state == 'pause'){
			$("i.control-status").removeClass('control-pause').removeClass('fa-pause').addClass('control-play fa-play');
		}
		if(mpd.random==1){
			$(".shuffle-toggle").removeClass('shuffle-off').addClass('shuffle-on');
		}
		else if(mpd.random==0){
			$(".shuffle-toggle").removeClass('shuffle-on').addClass('shuffle-off')
		}


	}
	$("h2.song-info").text(mpd.nowplaying.artist+"-"+mpd.nowplaying.album+" ("+mpd.nowplaying.date+")");
	if(mpd.nowplaying.title!==songtitle){
		songtitle = mpd.nowplaying.title;
		$("h1.song").text(mpd.nowplaying.title);
		setArtBackground(mpd.nowplaying.artist, mpd.nowplaying.album);
	}

	rate = mpd.audio.split(":");
	$("p.more-song-info").text(mpd.bitrate+' kbps / '+rate[0]/1000+'kHz / '+rate[1]+"bits / "+rate[2]+"channels");
	$("#ui-volume-slider").simpleSlider("setValue", mpd.volume);
	if(mpd.updating_db){
		$(".update-mpd-library").hide();
		$(".update-spinner").show();
	} else {
		$(".update-mpd-library").show();
		$(".update-spinner").hide();
	}
}

var cache = new LastFMCache();
var lastfm = new LastFM({
  apiKey    : '9a298fdf8a525f9ceb62801f6711038f',
  apiSecret : '2548772e5e1b6c0295f94b2cf07b5130',
  cache     : cache
});

function setArtBackground(npartist, npalbum){	/* Load some artist info. */
	if(npartist!=='undefined' && npalbum !== 'undefined'){
		lastfm.album.getInfo({artist: npartist, album: npalbum},{
			success: function(data){
				$("#headerwrap").css("background-image", "url("+data.album.image[4]["#text"]+")");
			},
			error: function(code, message){
	
			}
		});
		
	}
}

var library_data;

// Load the library once the page starts scrolling (either from manual scroll or from clicking library in the menu)
// Requests '/library.json' and then deals with it. Possible better front-end here for the library
$(document).ready(function(){
	$.ajax({
    	type: "GET",
    	url: '/library/',
    	dataType: 'json',
    	success: function(library){
    		showBaseLibrary(library);
    	}

	});	 
});

function getPlaylist(){
	$.ajax({
    	type: "GET",
    	url: '/playlist.json',
    	dataType: 'json',
    	success: function(playlist){
    		$("div.playlist-info").empty();
    		updatePlaylistDOM(playlist);
    		setTimeout(getPlaylist, 10000);
    	}

	});

}

var removeIcon =  "<i class='fa fa-remove playlist-control-remove'></i>";

function updatePlaylistDOM(playlist){
	$.each(playlist, function(i, item){
		try{
			if(item.hasOwnProperty('title')){
				$("div.playlist-info").append("<div class='playlist-item' data-id='"+item.id+"'>"+removeIcon+(parseInt(item.pos)+1)+'. '+item.title+"</div>");
			}
			else{
				$("div.playlist-info").append("<div class='playlist-library-1'>"+item.file+"</div>");	
			}
		}
		//any errors, just pass them... bit of a hack but whatever
		catch(err){}
	});

}

function searchLibrary(search_term){
	$("div.library-info").empty();
	$.ajax({
		type: "GET",
		url: '/library_search/'+search_term,
		dataType: 'json',
		success: function(library_search){
			showBaseLibrary(library_search);
			$("div.library-dir-up").attr("data-uri", '/');
		}
	});

};

function getLibraryDIR(dir){
	$("div.library-info").empty();
	if(typeof(Storage) !== "undefined") {
		if(localStorage.getItem(dir) !== "undefined"){
			$.ajax({
				type: "GET",
				url: '/library/'+dir,
				dataType: 'json',
				success: function(library){
					showBaseLibrary(library);
					$("div.library-dir-up").attr("data-uri", dir);
					if(typeof(Storage) !== "undefined") {
			 			localStorage.setItem(dir, library);   // Code for localStorage/sessionStorage.
					}else{
						
					}
				}
			});

		} else{
			showBaseLibrary(localStorage.getItem(dir));
			$("div.library-dir-up").attr("data-uri", dir);
		};
	}else {
		
	}

}

var music_icon = "<i class='fa fa-music library-control-play'></i>";
var add_icon ="<i class='fa fa-plus library-control library-control-add' data-toggle='tooltip' data-placement='top' title='Add to now playing'></i>";
var play_icon="<i class='fa fa-play-circle library-control library-control-play' data-placement='top' title='Replace and play' ></i>";

function showBaseLibrary(library){
	$.each(library, function(i, item){
		try{
			if(item.hasOwnProperty('title')){
				$("div.library-info").append('<div class="music-item" data-uri="'+item.file+'"><div class="music-library-icon">'+add_icon+play_icon+music_icon+'</div><div class="music-library-2">'+item.title+'</div></div>');	
			}
			else{
				$("div.library-info").append('<div class="music-library-1" data-uri="'+item.directory+'"><div class="music-library-icon">'+add_icon+play_icon+'</div><div class="music-library-2">'+item.directory+'</div></div>');
			}
		}
		//any errors, just pass them... bit of a hack but whatever
		catch(err){}
	});
	$('.library-control').tooltip();
}
