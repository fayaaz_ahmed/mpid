from mpd import MPDClient, ConnectionError

mpdclient = MPDClient()

def connect():
    
    mpdclient.timeout = 10
    mpdclient.idletimeout = None
    mpdclient.connect('localhost', 6600)
    return mpdclient.mpd_version

def disconnect():
    mpdclient.disconnect()

########################################
##         Library methods            ##
########################################
def update_library():
    return mpdclient.update()


def get_library():
    """ Get all library info """
    try:
        return mpdclient.listallinfo()
    except ConnectionError:
        connect()
        return get_library()
    
def lsinfo_library(library_dir):
    """
    Gets directory info, for browsing through the MPD library
    :param library_dir: Directory to search
    :return: Returns all items in that directory from the MPD library.
    """
    try:
        return mpdclient.lsinfo(library_dir)
    except ConnectionError:
        connect()
        return lsinfo_library(library_dir)
    
def search_library(search_term):
    """
    Search function for the library
    :param search_term: Search term used for searching any tag in the library
    :return: The results of searching for any tags containing the search term
    """
    try:
        return mpdclient.search('any', search_term)
    except ConnectionError:
        connect()
        return search_library(search_term)

########################################
##         Playlist methods           ##
########################################


def get_playlist():
    """
    :return: Get the current playlist as a list
    """
    try:
        return mpdclient.playlistinfo()
    except ConnectionError:
        connect()
        return get_playlist()


def add_playlist(uri):
    """
    :param uri: URI to add to playlist - can be directory or file
    :return: The playlist after adding the file(s) to the bottom of the playlist
    """
    try:
        mpdclient.add(uri)
        return get_playlist()
    except ConnectionError:
        connect()
        return add_playlist(uri)

def clear_playlist():
    try:
        mpdclient.clear()
        return get_playlist()
    except ConnectionError:
        connect()
        return clear_playlist()
    
def replace_playlist_and_play(uri):
    try:
        mpdclient.command_list_ok_begin()  
        mpdclient.clear()
        mpdclient.add(uri)
        mpdclient.play(0)
        mpdclient.playlistinfo()
        status = mpdclient.command_list_end()
        return status[3]
    except ConnectionError:
        connect()
        return replace_playlist_and_play(uri)
    
def remove_from_playlist(pos):
    try:
        mpdclient.deleteid(pos)
        return get_playlist()
    except ConnectionError:
        connect()
        return remove_from_playlist(pos)
    
########################################
##         Play methods               ##
########################################

    
def get_status():
    try:
        status = mpdclient.status()
        status['nowplaying'] = mpdclient.currentsong()
        return status
    except ConnectionError:
        connect()
        return get_status()
        
def next():
    try:
        mpdclient.next()
    except ConnectionError:
        connect()
        return next()
        
def previous():
    try:
        mpdclient.previous()
    except ConnectionError:
        connect()
        return previous()

def stop():
    try:
        mpdclient.stop()
    except ConnectionError:
        connect()
        return stop()
        
def playpause():
    try:
        if mpdclient.status()['state'] == 'stop':
            mpdclient.play(0)
        else:
            mpdclient.pause()
    except ConnectionError:
        connect()
        return playpause()
    
def set_volume(volume):
    try:
        mpdclient.setvol(volume)
    except ConnectionError:
        connect()
        return set_volume()

def seek(songpos, time):
    try:
        mpdclient.seek(songpos, time)
    except ConnectionError:
        connect()
        return seek(songpos, time)














def update():
    try:
        mpdclient.update()
    except ConnectionError:
        connect()
        return update()
