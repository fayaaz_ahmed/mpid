from flask import Flask, jsonify, Response, request
from werkzeug.contrib.fixers import ProxyFix
from mpdclient import client
from settings import devices, lastfm, wifi
import json

app = Flask(__name__, '/static')

import os

'''
#Just return the index.html page
@app.route('/')
def index():
    return app.send_static_file(os.path.join('templates','index.html'))
'''

# The JSON status of MPD
@app.route('/status.json')
def status():
    status = client.get_status()
    return jsonify(**status)


app.wsgi_app = ProxyFix(app.wsgi_app)

# JSON view of the entire library. Takes a while to generate on a weak machine! (Like the raspberrypi)
@app.route('/library.json')
def all_library():
    all_library_list = client.get_library()
    return Response(json.dumps(all_library_list), mimetype='application/json')


# URL for root library Returns all the items in the root
@app.route('/library/')
def root_library():
    library_info = client.lsinfo_library('/')
    return Response(json.dumps(library_info), mimetype='application/json')


# URL for all library items. Returns all the items in the MPD library specified by dir.
@app.route('/library/<path:folder>')
def library(folder):
    library_info = client.lsinfo_library(folder)
    return Response(json.dumps(library_info), mimetype='application/json')


# URL for searching library items. Returns search results for any tag.
@app.route('/library_search/<path:search>')
def library_search(search):
    search_info = client.search_library(search)
    return Response(json.dumps(search_info), mimetype='application/json')


# URL for current playlist
@app.route('/playlist.json')
def playlist():
    allplaylist = client.get_playlist()
    return Response(json.dumps(allplaylist), mimetype='application/json')


# URL for playlist methods. Requires POST.
@app.route('/playlist/<method>', methods=['POST'])
def playlist_post(method):
    if method == 'add':
        uri = request.values['uri']
        playlist = client.add_playlist(uri)
    if method == 'clear':
        playlist = client.clear_playlist()
    if method == 'replace':
        uri = request.values['uri']
        playlist = client.replace_playlist_and_play(uri)
    if method == 'remove':
        songid = request.values['songid']
        playlist = client.remove_from_playlist(songid)
    if method == 'youtube':
        ytstring = request.values['ytstring']
        mpduri = ytclient.get_mpd_youtube_uri(ytstring)
        playlist = client.add_playlist(mpduri)

    return Response(json.dumps(playlist), mimetype='application/json')


# URL for settings. This could possibly be split up as more settings are added
@app.route('/settings/<subsetting>', methods=['POST', 'GET'])
def settings(subsetting):
    if subsetting == 'audio_devices':
        if request.method == 'GET':
            audiodevices = devices.get_devices()
            return Response(json.dumps(audiodevices), mimetype='application/json')
        if request.method == 'POST':
            devices.set_devices(request.values['hw'], request.values['index'])
            return Response(json.dumps({'success': 'true'}), mimetype='application/json')
    elif subsetting == 'lastfm':
        if request.method == 'POST':
            if lastfm.set_lastfm(request.values['username'], request.values['password']):
                return Response(json.dumps({'success': 'true'}), mimetype='application/json')
    elif subsetting == 'wifi':
        if request.method == 'POST':
            if wifi.set_wifi(request.values['ssid'], request.values['passphrase']):
                return Response(json.dumps({'success': 'true'}), mimetype='application/json')



# URL for playback control. Must be a POST request
@app.route('/control/', methods=['POST'])
def control():
    if request.values['method'] == 'stop':
        client.stop()
    elif request.values['method'] == 'playpause':
        client.playpause()
    elif request.values['method'] == 'next':
        client.next()
    elif request.values['method'] == 'previous':
        client.previous()
    elif request.values['method'] == 'volume':
        volume = request.values['data']
        client.set_volume(volume)
    elif request.values['method'] == 'update':
        client.update()
    elif request.values['method'] == 'seek':
        time = request.values['data-time']
        songposition = request.values['data-song']
        client.seek(songposition, time)

    status = client.get_status()
    return jsonify(**status)



if __name__ == '__main__':
    app.run(host="127.0.0.1", port=8000, debug=True)
    
