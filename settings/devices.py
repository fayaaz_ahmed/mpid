from subprocess import Popen, PIPE, call
from jinja2 import Environment, FileSystemLoader

def get_devices():
    output,error = Popen(["aplay", "-l"], stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()
    
    devicesList = []
    for line in output.split('\n'):
        if 'card' in line:
            lineSplitColon = line.split(':')
            devicesList.append(lineSplitColon[0] + lineSplitColon[1].split(',')[1] + lineSplitColon[2])
            
    return devicesList

def set_devices(hw, index):

    env = Environment(loader=FileSystemLoader('static/templates/'))
    template = env.get_template('mpd.conf')
    new_conf = template.render(hw=hw , index=index)
    
    with open("/etc/mpd.conf", "wb") as fh:
        fh.write(new_conf)
        
    call(["/etc/init.d/mpd", "restart"])
    
    return True