from subprocess import call
from jinja2 import Environment, FileSystemLoader
import hashlib

def set_wifi(SSID, passphrase):

    env = Environment(loader=FileSystemLoader('static/templates/'))
    template = env.get_template('wpa.config')
    new_conf = template.render(SSID=SSID, passphrase=passphrase)

    with open("/etc/wpa.config", "wb") as fh:
        fh.write(new_conf)

    call(["ifdown", "wlan0", "wlan1"])
    call(["ifup", "wlan0", "wlan1"])

    return True