from subprocess import call
from jinja2 import Environment, FileSystemLoader
import hashlib

def set_lastfm(username, password):

    env = Environment(loader=FileSystemLoader('static/templates/'))
    template = env.get_template('mpdscribble.conf')
    md5password = hashlib.md5(password).hexdigest()

    new_conf = template.render(username=username, password=md5password)

    with open("/etc/mpdscribble.conf", "wb") as fh:
        fh.write(new_conf)

    call(["/etc/init.d/mpdscribble", "restart"])

    return True